readRoster = require "../src/read-roster.coffee"

describe "Reading JSON txt files", ->
  read = readRoster "studentRoster.txt"

  describe "returning a lookup object", ->
    it "should be an object object", ->
      expect(read).to.be.an "object"

    it "should have correct values in an array for a given key", ->
      expect(read["30182"]).to.have.members [126858]
      expect(read["6042"]).to.have.members [128551]
      expect(read["55555"]).to.have.members [128226]
