fs = require "fs"

removeBadLines = (line) ->
  line? and typeof line is "string" and line isnt ""

getIdValue = (obj) ->
  obj.id or obj.teacher_id or obj.student_id

getValues = (obj) ->
  Object.keys(obj).reduce ((result, key) ->
    if getIdValue(obj) isnt obj[key] and obj.hasOwnProperty(key)
      result.push obj[key]
    result
  ), []

module.exports = (fileName) ->
  contents = fs.readFileSync "test-case/#{fileName}",
    encoding: "utf-8"
  contents
    .split("\n")
    .filter(removeBadLines)
    .map(JSON.parse)
    .reduce ((result, obj) ->
      result[getIdValue(obj)] = getValues(obj)
      result
    ), Object.create(null)
